package days2019;

import java.io.IOException;
import java.util.List;

import utils.FileReaderHelper;

/**
 * Day 1 of Advent of Code
 *
 */
public final class Day1 {
	
	private void run() throws IOException {
		List<Integer> list = FileReaderHelper.readFileToInt("input2019/day1");
		
		int Part1Total = list.stream().mapToInt(i -> calculateFuel(i)).sum();
		
		System.out.println(Part1Total);
		
		int Part2Total = list.stream().mapToInt(i -> calculateTotalfuel(i)).sum();
		
		System.out.println(Part2Total);
	}
	
	private int calculateFuel(int mass) {
		
		int divided = mass / 3;
		return divided -2;
		
	}

	private int calculateTotalfuel(int mass) {
		int requiredFuel = calculateFuel(mass);

		if (requiredFuel <= 0) {
			return 0;
		}
		
		return requiredFuel + calculateTotalfuel(requiredFuel);

	}
	
	public static void main(String[] args) throws IOException {
		Day1 day1 = new Day1();
		day1.run();
	}

}
