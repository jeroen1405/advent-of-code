package days2019;

import java.io.IOException;
import java.util.List;

import utils.FileReaderHelper;

public class Day2 {
	
	private void run() throws IOException {
		List<Integer> list = FileReaderHelper.readFileToInt("input2019/day2");
		
		// Step 1
		
		list.set(1, 12);
		list.set(2, 2);
		
		Intprocessor processor = new Intprocessor(list);
		processor.printList();
		processor.run();	
		processor.printList(); // returns 3101878
		
		// Step 2
		
		int expectedOutput = 19690720;

		OUTER_LOOP:
		for (int i = 0; i <= 99; i++) {
			for (int j = 0; j <= 99; j++) {
				list.set(1, i);
				list.set(2, j);
				Intprocessor processor2 = new Intprocessor(list);
				processor2.run();
				processor2.printList();
				if (processor2.getResult().get(0).intValue() == expectedOutput) {
					System.out.println(String.format("Found with value %d and %d", i, j));
					break OUTER_LOOP;
				}
			}
		}
	}
	
	public static void main(String[] args) throws IOException {
		Day2 day2 = new Day2();
		day2.run();
	}

}
