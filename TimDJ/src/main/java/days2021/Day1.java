package days2021;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import utils.FileReaderHelper;

/**
 * Day 1 of Advent of Code 2021
 */
public final class Day1 {

	private void run() throws IOException {
		List<Integer> list = FileReaderHelper.readFileToInt("input2021/day1");
		long anwser = method1(list);
		System.out.println(anwser);
		long anwser2 = method2(list);
		System.out.println(anwser2);
	}

	private long method1(List<Integer> list) {
		return IntStream.range(0, list.size() - 1)
				.filter(i -> list.get(i) < list.get(i + 1))
				.count();
	}

	private long method2(List<Integer> list) {
		List<Long> sumMeasurements = IntStream.range(0, list.size() - 2)
				.mapToLong(i -> list.get(i) + list.get(i + 1) + list.get(i + 2))
				.boxed()
				.collect(Collectors.toList());

		return IntStream.range(0, sumMeasurements.size() - 1)
				.filter(i -> sumMeasurements.get(i) < sumMeasurements.get(i + 1))
				.count();
	}

	public static void main(String[] args) throws IOException {
		Day1 day = new Day1();
		day.run();
	}
}
