package days2020;

import java.io.IOException;
import java.util.List;

import utils.FileReaderHelper;

/**
 * Day 5 of Advent of Code 2020
 *
 */
public final class Day5 {

    private void run() throws IOException {
        List<String> list = FileReaderHelper.readFile("input2020/day5");
        int highest = list.stream().mapToInt(s -> getId(s)).max().orElse(0);
        System.out.println(highest);

        int chairId = findChairId(list);
        System.out.println(chairId);
    }

    private int findChairId(List<String> list) {
        int[] ids = list.stream().mapToInt(s -> getId(s)).sorted().toArray();
        for (int i = 1; i < ids.length; i++) {
            System.out.println(ids[i]);
            if (ids[i] - ids[i - 1] > 1) {
                return ids[i] - 1;
            }
        }
        return 0;
    }

    private int getId(String boardingpass) {
        int rowNr = getNumber(0, 127, boardingpass.substring(0, 7));
        int columnNr = getNumber(0, 7, boardingpass.substring(7));
        return rowNr * 8 + columnNr;
    }

    int getNumber(int rangeMin, int rangeMax, String binary) {
        if (rangeMin == rangeMax) {
            return rangeMax;
        }

        char value = binary.charAt(0);
        int middle = (rangeMin + rangeMax) / 2;

        if (value == 'B' || value == 'R') {
            return getNumber(middle + 1, rangeMax, binary.substring(1));
        } else {
            return getNumber(rangeMin, middle, binary.substring(1));
        }
    }

    public static void main(String[] args) throws IOException {
        Day5 day = new Day5();
        day.run();
    }
}
