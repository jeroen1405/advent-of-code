package days2020;

import java.io.IOException;
import java.util.List;

import days2020.helpclasses.EGeoObject;
import days2020.helpclasses.ToboggganMapUtils;
import utils.FileReaderHelper;

/**
 * Day 3 of Advent of Code 2020
 *
 */
public final class Day3 {

	private void run() throws IOException {
		List<String> list = FileReaderHelper.readFile("input2020/day3");

		long trees1 = ToboggganMapUtils.getAllObjects(list, 1, 1).stream().filter(o -> o == EGeoObject.TREE).count();
		long trees2 = ToboggganMapUtils.getAllObjects(list, 3, 1).stream().filter(o -> o == EGeoObject.TREE).count();
		long trees3 = ToboggganMapUtils.getAllObjects(list, 5, 1).stream().filter(o -> o == EGeoObject.TREE).count();
		long trees4 = ToboggganMapUtils.getAllObjects(list, 7, 1).stream().filter(o -> o == EGeoObject.TREE).count();
		long trees5 = ToboggganMapUtils.getAllObjects(list, 1, 2).stream().filter(o -> o == EGeoObject.TREE).count();

		System.out.println(trees1);
		System.out.println(trees2);
		System.out.println(trees3);
		System.out.println(trees4);
		System.out.println(trees5);
		
		System.out.println(trees1 * trees2 * trees3 * trees4 * trees5);
	}

	public static void main(String[] args) throws IOException {
		Day3 day1 = new Day3();
		day1.run();
	}
}
