package days2020;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import days2020.helpclasses.Chair;
import utils.FileReaderHelper;

/**
 * Day 11 of Advent of Code 2020
 *
 */
public final class Day11 {

    private void run() throws IOException {
        List<String> list = FileReaderHelper.readFile("input2020/day11");
        Chair[][] chairs = createChairGrid(list);
        List<Chair> chairsList = createChairsList1(chairs);

         setToFinalPosition(chairsList, 4);
         int anwser = countSeats(chairsList);
        
         System.out.println(anwser);

        Chair[][] chairs2 = createChairGrid(list);
        List<Chair> chairsList2 = createChairsList2(chairs2);

        setToFinalPosition(chairsList2, 5);
        int anwser2 = countSeats(chairsList2);

        System.out.println(anwser2);
    }

    private void setToFinalPosition(List<Chair> chairsList, int maxOccupied) {
        int chairsChanged = 1;
        int runs = 0;
        while (chairsChanged > 0) {
            runs++;
            chairsChanged = 0;

            for (Chair chair : chairsList) {
                int occupiedAdjecent = chair.countAdjecentOccupied();
                if (chair.isOccupied() && occupiedAdjecent >= maxOccupied) {
                    chair.setNewState(false);
                    chairsChanged++;
                }

                if (!chair.isOccupied() && occupiedAdjecent == 0) {
                    chair.setNewState(true);
                    chairsChanged++;
                }
            }

            chairsList.stream().forEach(c -> c.commit());
           // printlist(chairsList);
        }

        System.out.println(runs);

    }

    private void printlist(List<Chair> chairsList) {
        String[] list = new String[10];
        for (int i = 0; i < list.length; i++) {
            list[i] = new String("..........");
        }

        for (Chair chair : chairsList) {
                char replace = chair.isOccupied() ? '#' : 'L';
                char[] chars = list[chair.getPositionY()].toCharArray();
                chars[chair.getPositionX()] = replace;
                list[chair.getPositionY()] = String.copyValueOf(chars);
        }
        
        for (int i = 0; i < list.length; i++) {
            System.out.println(list[i]);
        }

        try (Scanner scanner = new Scanner(System.in)) {
            scanner.nextLine();
        }

    }

    private int countSeats(List<Chair> chairsList) {
        return (int)chairsList.stream().filter(c -> c.isOccupied()).count();
    }

    private Chair[][] createChairGrid(List<String> list) {
        int y = list.size();
        int x = list.get(0).length();

        Chair[][] chairs = new Chair[y][x];

        for (int i = 0; i < y; i++) {
            String line = list.get(i);
            for (int j = 0; j < x; j++) {
                if (line.charAt(j) == 'L') {
                    chairs[i][j] = new Chair(i, j);
                }
            }

        }
        return chairs;
    }

    private List<Chair> createChairsList1(Chair[][] chairs) {
        int y = chairs.length;
        int x = chairs[0].length;

        for (int i = 0; i < y; i++) {
            for (int j = 0; j < x; j++) {
                if (chairs[i][j] != null) {
                    for (int k = i - 1; k <= i + 1; k++) {
                        for (int l = j - 1; l <= j + 1; l++) {
                            if ((k == i && l == j) || k < 0 || l < 0 || k >= y || l >= x) {
                                // skip
                                continue;
                            }
                            if (chairs[k][l] != null) {
                                chairs[i][j].addAdjecentChair(chairs[k][l]);
                            }
                        }

                    }
                }

            }
        }

        List<Chair> chairList = new ArrayList<>();
        for (Chair[] chairLine : chairs) {
            chairList.addAll(Arrays.asList(chairLine));
        }

        return chairList.stream().filter(c -> c != null).collect(Collectors.toList());
    }

    private List<Chair> createChairsList2(Chair[][] chairs) {
        int y = chairs.length;
        int x = chairs[0].length;
        for (int i = 0; i < y; i++) {
            for (int j = 0; j < x; j++) {
                if (chairs[i][j] != null) {
                    Chair up = findChairUp(chairs, i, j);
                    Chair down = findChairDown(chairs, i, j);
                    Chair left = findChairLeft(chairs, i, j);
                    Chair right = findChairRight(chairs, i, j);
                    Chair upLeft = findChairUpLeft(chairs, i, j);
                    Chair upRight = findChairUpRight(chairs, i, j);
                    Chair downLeft = findChairDownLeft(chairs, i, j);
                    Chair downRight = findChairDownRight(chairs, i, j);

                    if (up != null) {
                        chairs[i][j].addAdjecentChair(up);
                    }
                    if (down != null) {
                        chairs[i][j].addAdjecentChair(down);
                    }
                    if (left != null) {
                        chairs[i][j].addAdjecentChair(left);
                    }
                    if (right != null) {
                        chairs[i][j].addAdjecentChair(right);
                    }
                    if (upLeft != null) {
                        chairs[i][j].addAdjecentChair(upLeft);
                    }
                    if (upRight != null) {
                        chairs[i][j].addAdjecentChair(upRight);
                    }
                    if (downLeft != null) {
                        chairs[i][j].addAdjecentChair(downLeft);
                    }
                    if (downRight != null) {
                        chairs[i][j].addAdjecentChair(downRight);
                    }

                }

            }

        }

        List<Chair> chairList = new ArrayList<>();
        for (Chair[] chairLine : chairs) {
            chairList.addAll(Arrays.asList(chairLine));
        }

        return chairList.stream().filter(c -> c != null).collect(Collectors.toList());
    }

    private Chair findChairUp(Chair[][] chairs, int y, int x) {
        if (y == 0) {
            return null;
        }

        for (int i = y - 1; i >= 0; i--) {
            Chair chair = chairs[i][x];
            if (chair != null) {
                return chair;
            }
        }
        return null;
    }

    private Chair findChairDown(Chair[][] chairs, int y, int x) {
        if (y == chairs.length - 1) {
            return null;
        }

        for (int i = y + 1; i < chairs.length; i++) {
            Chair chair = chairs[i][x];
            if (chair != null) {
                return chair;
            }
        }

        return null;
    }

    private Chair findChairLeft(Chair[][] chairs, int y, int x) {
        if (x == 0) {
            return null;
        }

        for (int i = x - 1; i >= 0; i--) {
            Chair chair = chairs[y][i];
            if (chair != null) {
                return chair;
            }
        }
        return null;
    }

    private Chair findChairRight(Chair[][] chairs, int y, int x) {
        if (x == chairs[0].length - 1) {
            return null;
        }

        for (int i = x + 1; i < chairs[0].length; i++) {
            Chair chair = chairs[y][i];
            if (chair != null) {
                return chair;
            }
        }
        return null;
    }

    private Chair findChairUpLeft(Chair[][] chairs, int y, int x) {
        if (y == 0 || x == 0) {
            return null;
        }

        int lowest = y < x ? y : x;
        int adjustment = 0;

        for (int i = 0; i < lowest; i++) {
            adjustment++;
            Chair chair = chairs[y - adjustment][x - adjustment];
            if (chair != null) {
                return chair;
            }
        }
        return null;
    }

    private Chair findChairUpRight(Chair[][] chairs, int y, int x) {
        if (y == 0 || x == chairs[0].length - 1) {
            return null;
        }

        int distanceToBorderY = y;
        int distanceToBorderX = chairs[0].length - 1 - x;

        int lowest = distanceToBorderY < distanceToBorderX ? distanceToBorderY : distanceToBorderX;
        int adjustment = 0;

        for (int i = 0; i < lowest; i++) {
            adjustment++;
            Chair chair = chairs[y - adjustment][x + adjustment];
            if (chair != null) {
                return chair;
            }
        }

        return null;
    }

    private Chair findChairDownLeft(Chair[][] chairs, int y, int x) {
        if (y == chairs[0].length - 1 || x == 0) {
            return null;
        }

        int distanceToBorderY = chairs.length - 1 - y;
        int distanceToBorderX = x;

        int lowest = distanceToBorderY < distanceToBorderX ? distanceToBorderY : distanceToBorderX;

        int adjustment = 0;
        
        for (int i = 0; i < lowest; i++) {
            adjustment++;
            Chair chair = chairs[y + adjustment][x - adjustment];
            if (chair != null) {
                return chair;
            }
        }
        return null;
    }

    private Chair findChairDownRight(Chair[][] chairs, int y, int x) {
        if (y == chairs[0].length - 1 || x == chairs[0].length - 1) {
            return null;
        }

        int distanceToBorderY = chairs.length - 1 - y;
        int distanceToBorderX = chairs[0].length - 1 - x;
        int lowest = distanceToBorderY < distanceToBorderX ? distanceToBorderY : distanceToBorderX;

        int adjustment = 0;

        for (int i = 0; i < lowest; i++) {
            adjustment++;
            Chair chair = chairs[y + adjustment][x + adjustment];
            if (chair != null) {
                return chair;
            }
        }
        return null;
    }

    public static void main(String[] args) throws IOException {
        Day11 day = new Day11();
        day.run();
    }
}
