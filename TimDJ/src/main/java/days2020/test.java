/*
 (C) COPYRIGHT 2020 TECHNOLUTION BV, GOUDA NL
| =======          I                   ==          I    =
|    I             I                    I          I
|    I   ===   === I ===  I ===   ===   I  I    I ====  I   ===  I ===
|    I  /   \ I    I/   I I/   I I   I  I  I    I  I    I  I   I I/   I
|    I  ===== I    I    I I    I I   I  I  I    I  I    I  I   I I    I
|    I  \     I    I    I I    I I   I  I  I   /I  \    I  I   I I    I
|    I   ===   === I    I I    I  ===  ===  === I   ==  I   ===  I    I
|                 +---------------------------------------------------+
+----+            |  +++++++++++++++++++++++++++++++++++++++++++++++++|
     |            |             ++++++++++++++++++++++++++++++++++++++|
     +------------+                          +++++++++++++++++++++++++|
                                                        ++++++++++++++|
                                                                 +++++|
 */
package days2020;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

/**
 * TODO tim.de.jong comment this
 */
public class test {
    
    public static Scanner reader= new Scanner(System.in);
    public static String[] GetAll(String st){
        String[] list = st.split("bags contain \\d | bags, \\d | bag, \\d | bags\\.| bag\\.");
        if(list.length>1) {
            return Arrays.copyOfRange(list, 1, list.length);
        }
        return new String[0];
    }

    public static String GetName(String st){
        return st.split("bags")[0].stripTrailing();
    }

    static boolean isInArr(String[] arr,String looking){
        for (String s:arr){
            if(s.strip().equals(looking.strip())){
                return true;
            }
        }
        return false;
    }
    public static int cnt(HashMap<String,String[]> hm, String color, int count){//part1
        for(String key:hm.keySet()){
            // if the key contains the color were looking for
            if(isInArr(hm.get(key),color)){
                //add one to count
                //search for al.f.l of the key appearences
                System.out.println(key+" "+count);
                hm.replace(key,new String[0]);
                count+=cnt(hm,key,0)+1;
            }
        }
        return count;
    }
    public static int cnt2(HashMap<String,String[]> hm,  int count, String name) {//part2
        for(String s: hm.get(name)) {//find shiny gold and traverse through the string array and is error here "null"
            if(isInArr(hm.get("shiny gold"),s)){//check that s exists
                count++;//count bag
                hm.replace(s,new String[0]);
                count+=cnt2(hm,0,s)+1;//recursion through the bag
            }
        }
        return count;
    }
    public static void main(String[] args) {
        HashMap<String,String[]> hm = new HashMap<>();
        HashMap<String,String[]> hm2 = new HashMap<>();
        String input=reader.nextLine();
        while(!input.equals("-1")){//placing into hm
            hm.put(GetName(input),GetAll(input));
            input=reader.nextLine();
        }
        System.out.println("");
        int c =cnt(hm,"shiny gold",0);
        System.out.println(c);
        int cc= cnt2(hm,0,"shiny gold");
        System.out.println(cc);   
    }
    public static void printArr(String[] arr){
        if(arr==null){
            System.out.println("null");
            return;
        }
        for(String s: arr){
            System.out.print(s+", ");
        }
        System.out.println("");
    }

}
