package days2020;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.LongStream;

import utils.FileReaderHelper;

/**
 * Day 14 of Advent of Code 2020
 *
 */
public final class Day14 {

    private void run() throws IOException {
        List<String> list = FileReaderHelper.readFile("input2020/day14");
        long sum = findSum(list);
        System.out.println(sum);
        long sum2 = findSum2(list);
        System.out.println(sum2);
    }

    private long findSum(List<String> list) {
        long[] fullList = new long[100000];
        String mask = "";

        for (String line : list) {
            if (line.substring(0, 4).equals("mask")) {
                mask = parseNewMask(line);
            } else {
                int index = getIndex(line);
                long value = getValue(line, mask);
                fullList[index] = value;
            }
        }

        return LongStream.of(fullList).sum();
    }

    private long findSum2(List<String> list) {
        Map<Long, Long> map = new HashMap<>();
        String mask = "";

        for (String line : list) {
            if (line.substring(0, 4).equals("mask")) {
                mask = parseNewMask(line);
            } else {
                long[] indexes = getIndexes(line, mask);
                long value = getValue(line);
                for (long l : indexes) {
                    map.put(l, value);
                }
            }
        }

        return map.values().stream().mapToLong(l -> l).sum();
    }

    private int getValue(String line) {
        return Integer.parseInt(line.split("=")[1].substring(1));
    }

    private long getValue(String line, String mask) {
        StringBuilder builder = new StringBuilder(getBinary(getValue(line)));

        for (int i = 0; i < 36; i++) {
            char maskChar = mask.charAt(i);
            if (maskChar == 'X') {
                continue;
            }
            builder.setCharAt(i, maskChar);
        }

        return Long.parseLong(builder.toString(), 2);
    }

    private List<String> parseMask(String line, String mask) {

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < line.length(); i++) {
            char maskChar = mask.charAt(i);
            if (maskChar == 'X') {

                List<String> values = parseMask(line.substring(i + 1), mask.substring(i + 1));
                List<String> result = new ArrayList<>();
                String finalString = builder.toString();

                for (String value : values) {
                    result.add(finalString + "0" + value);
                    result.add(finalString + "1" + value);
                }
                return result;
            }
            
            if (maskChar == '1') {
                builder.append(1);
            } else {
                builder.append(line.charAt(i));
            }
        }

        return Arrays.asList(builder.toString());
    }

    private long[] getIndexes(String line, String mask) {
        List<String> result = parseMask(getBinary(getIndex(line)), mask);
        return result.stream().mapToLong(l -> Long.parseLong(l, 2)).toArray();
    }

    private String getBinary(int value) {
        String binaryString = Integer.toBinaryString(value);
        return String.format("%36s", binaryString).replace(' ', '0');
    }

    private int getIndex(String line) {
        return Integer.parseInt(line.substring(4).split("]")[0]);
    }

    private String parseNewMask(String line) {
        return line.substring(7);
    }

    public static void main(String[] args) throws IOException {
        Day14 day = new Day14();
        day.run();
    }
}
