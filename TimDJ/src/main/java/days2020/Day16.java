package days2020;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import days2020.helpclasses.ValidationRule;
import utils.FileReaderHelper;

/**
 * Day 1 of Advent of Code 2020
 *
 */
public final class Day16 {

    private void run() throws IOException {
        List<String> list = FileReaderHelper.readFile("input2020/day16");

        List<ValidationRule> validationRules = createValidationRules(list);
        String myTicket = list.get(22);
        List<String> nearbyTickets = list.subList(25, list.size());

        int errorRate = findErrors(nearbyTickets, validationRules);
        System.out.println(errorRate);

        List<String> validtickets = discardInvalidTickets(nearbyTickets, validationRules);
        Map<ValidationRule, Integer> mappedRules = mapRules(validtickets, validationRules);
        
        mappedRules.entrySet().stream().forEach(s -> System.out.println(s.getKey().getName() + " - " + s.getValue()));
        
        long value = findValue(mappedRules, myTicket);
        System.out.println(value);
        
    }

    private long findValue(Map<ValidationRule, Integer> mappedRules, String myTicket) {
        String[] ticket = myTicket.split(",");
        long sum = 1;
        
        for (Entry<ValidationRule, Integer> entry : mappedRules.entrySet()) {
            if (entry.getKey().getName().contains("departure")) {
               sum = sum * Integer.valueOf(ticket[entry.getValue()]);
            }
        }
        return sum;
    }

    private Map<ValidationRule, Integer> mapRules(List<String> validtickets, List<ValidationRule> validationRules) {
        List<int[]> values = new ArrayList<>();

        for (int i = 0; i < validationRules.size(); i++) {
            values.add(new int[190]);
        }

        for (int i = 0; i < validtickets.size(); i++) {
            String[] ticket = validtickets.get(i).split(",");
            for (int j = 0; j < ticket.length; j++) {
                int value = Integer.parseInt(ticket[j]);
                values.get(j)[i] = value;
            }
        }
        
        int a = 0;
        for (int[] is : values) {
            System.out.println("--------------------------------------");
            System.out.println(a++);
            for (int is2 : is) {
                System.out.print(is2+",");
            }
            System.out.println();
        }
        
        Map<ValidationRule, List<Integer>> possabilities = new HashMap<>();

        for (int i = 0; i < validationRules.size(); i++) {
            ValidationRule rule = validationRules.get(i);
            List<Integer> possibleValues = new ArrayList<>();
            for (int j = 0; j < validationRules.size(); j++) {
                boolean invalid = Arrays.stream(values.get(j)).anyMatch(v -> !rule.isValueValid(v));
                System.out.println(j + ": " + !invalid);
                if (!invalid) {
                    possibleValues.add(j);
                }
            }

            possabilities.put(rule, possibleValues);
        }

        Map<ValidationRule, Integer> mappedRules = new HashMap<>();
        boolean allmapped = false;
        while (!allmapped) {
            System.out.println("--------------------------------------");
            List<Integer> mappedNumbers = new ArrayList<>();
            List<ValidationRule> mappedValidations = new ArrayList<>();

            for (Entry<ValidationRule, List<Integer>> entry : possabilities.entrySet()) {
                System.out.println(entry.getKey().getName() + " possabilities");
                entry.getValue().stream().forEach(s -> System.out.print(s+ ","));
                System.out.println();
                if (entry.getValue().size() == 1) {
                    int index = entry.getValue().get(0);
                    mappedRules.put(entry.getKey(), index);
                    mappedNumbers.add(index);
                    mappedValidations.add(entry.getKey());
                    System.out.println("mapped " + entry.getKey().getName() +  " to " + index);
                }
            }
            
            for (ValidationRule rule : mappedValidations) {
                possabilities.remove(rule);
            }
            
            for (List<Integer> nrs : possabilities.values()) {
                nrs.removeAll(mappedNumbers);
            }
            
            if (possabilities.size() == 0) {
                allmapped = true;
            }

        }

        return mappedRules;
    }

    private List<String> discardInvalidTickets(List<String> nearbyTickets, List<ValidationRule> validationRules) {
        return nearbyTickets.stream()
                .filter(t -> Arrays.stream(t.split(","))
                        .mapToInt(s -> Integer.parseInt(s))
                        .filter(s -> !validateValue(s, validationRules))
                        .count() == 0)
                .collect(Collectors.toList());
    }

    private int findErrors(List<String> nearbyTickets, List<ValidationRule> validationRules) {
        int total = 0;
        for (String ticket : nearbyTickets) {
            total += Arrays.stream(ticket.split(","))
                    .mapToInt(s -> Integer.parseInt(s))
                    .filter(s -> !validateValue(s, validationRules))
                    .sum();
        }

        return total;
    }

    private boolean validateValue(int value, List<ValidationRule> validationRules) {
        for (ValidationRule validationRule : validationRules) {
            if (validationRule.isValueValid(value)) {
                return true;
            }
        }
        return false;
    }

    private List<ValidationRule> createValidationRules(List<String> list) {
        List<ValidationRule> validationRules = new ArrayList<>();
        Pattern pattern = Pattern.compile("([a-z ]+): (\\d+)-(\\d+) or (\\d+)-(\\d+)");
        for (int i = 0; i < 20; i++) {
            Matcher m = pattern.matcher(list.get(i));
            m.matches();

            ValidationRule rule = new ValidationRule(m.group(1), toInt(m.group(2)), toInt(m.group(3)),
                    toInt(m.group(4)), toInt(m.group(5)));

            validationRules.add(rule);
        }
        return validationRules;
    }

    private int toInt(String string) {
        return Integer.parseInt(string);
    }

    public static void main(String[] args) throws IOException {
        Day16 day = new Day16();
        day.run();
    }
}
