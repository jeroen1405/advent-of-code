/*
 (C) COPYRIGHT 2020 TECHNOLUTION BV, GOUDA NL
| =======          I                   ==          I    =
|    I             I                    I          I
|    I   ===   === I ===  I ===   ===   I  I    I ====  I   ===  I ===
|    I  /   \ I    I/   I I/   I I   I  I  I    I  I    I  I   I I/   I
|    I  ===== I    I    I I    I I   I  I  I    I  I    I  I   I I    I
|    I  \     I    I    I I    I I   I  I  I   /I  \    I  I   I I    I
|    I   ===   === I    I I    I  ===  ===  === I   ==  I   ===  I    I
|                 +---------------------------------------------------+
+----+            |  +++++++++++++++++++++++++++++++++++++++++++++++++|
     |            |             ++++++++++++++++++++++++++++++++++++++|
     +------------+                          +++++++++++++++++++++++++|
                                                        ++++++++++++++|
                                                                 +++++|
 */
package days2020.helpclasses;

/**
 * Validation Rules Day 16
 */
public final class ValidationRule {
    
    private final String name;
    private final int range1Min;
    private final int range1Max;
    private final int range2Min;
    private final int range2Max;

    public ValidationRule(String name, int range1Min, int range1Max, int range2Min, int range2Max) {
        this.name = name;
        this.range1Min = range1Min;
        this.range1Max = range1Max;
        this.range2Min = range2Min;
        this.range2Max = range2Max;
    }
    
    public boolean isValueValid(int value) {
        boolean valueInRange1 = value >= range1Min && value <= range1Max;
        boolean valueInRange2 = value >= range2Min && value <= range2Max;
        return valueInRange1 || valueInRange2;
    }

    public String getName() {
        return name;
    }

    public int getRange1Min() {
        return range1Min;
    }

    public int getRange1Max() {
        return range1Max;
    }

    public int getRange2Min() {
        return range2Min;
    }

    public int getRange3Max() {
        return range2Max;
    }
}
