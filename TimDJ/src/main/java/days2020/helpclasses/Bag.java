/*
 (C) COPYRIGHT 2020 TECHNOLUTION BV, GOUDA NL
| =======          I                   ==          I    =
|    I             I                    I          I
|    I   ===   === I ===  I ===   ===   I  I    I ====  I   ===  I ===
|    I  /   \ I    I/   I I/   I I   I  I  I    I  I    I  I   I I/   I
|    I  ===== I    I    I I    I I   I  I  I    I  I    I  I   I I    I
|    I  \     I    I    I I    I I   I  I  I   /I  \    I  I   I I    I
|    I   ===   === I    I I    I  ===  ===  === I   ==  I   ===  I    I
|                 +---------------------------------------------------+
+----+            |  +++++++++++++++++++++++++++++++++++++++++++++++++|
     |            |             ++++++++++++++++++++++++++++++++++++++|
     +------------+                          +++++++++++++++++++++++++|
                                                        ++++++++++++++|
                                                                 +++++|
 */
package days2020.helpclasses;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Hold bags
 */
public class Bag {
    
    private final Map<Bag, Integer> children = new HashMap<>();
    private final List<Bag> parents = new ArrayList<>();
    private final String name;
    
    
    public Bag(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public void addChild(Bag bag, int amount) {
        children.put(bag, amount);
    }
    
    public void addParent(Bag bag) {
        parents.add(bag);
    }
    
    public Map<Bag, Integer> getChildren() {
        return children;
    }
    
    public List<Bag> getParents() {
        return parents;
    }
    
    public List<Bag> getAllParent() {
        List<Bag> allParents = new ArrayList<>();
        allParents.addAll(parents);        
        
        for (Bag parent : parents) {
            allParents.addAll(parent.getAllParent());
        }
        
        return allParents;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(name);
        sb.append(" bags contains: ");
        
        for (Map.Entry<Bag, Integer> entry : children.entrySet()) {
            sb.append(entry.getValue());
            sb.append(" ");
            sb.append(entry.getKey().getName());
            sb.append(", ");
        }
        
        sb.append(" and is contained in: ");
        for (Bag bag : parents) {
            sb.append(bag.getName());
            sb.append(", ");
        }
        
        return sb.toString();
    }
}
