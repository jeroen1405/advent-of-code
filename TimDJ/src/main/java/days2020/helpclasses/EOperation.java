/*
 (C) COPYRIGHT 2020 TECHNOLUTION BV, GOUDA NL
| =======          I                   ==          I    =
|    I             I                    I          I
|    I   ===   === I ===  I ===   ===   I  I    I ====  I   ===  I ===
|    I  /   \ I    I/   I I/   I I   I  I  I    I  I    I  I   I I/   I
|    I  ===== I    I    I I    I I   I  I  I    I  I    I  I   I I    I
|    I  \     I    I    I I    I I   I  I  I   /I  \    I  I   I I    I
|    I   ===   === I    I I    I  ===  ===  === I   ==  I   ===  I    I
|                 +---------------------------------------------------+
+----+            |  +++++++++++++++++++++++++++++++++++++++++++++++++|
     |            |             ++++++++++++++++++++++++++++++++++++++|
     +------------+                          +++++++++++++++++++++++++|
                                                        ++++++++++++++|
                                                                 +++++|
 */
package days2020.helpclasses;

/**
 * Operations of the Accumulator
 */
public enum EOperation {
    ACCUMULATOR("acc"),
    JUMP("jmp"),
    NO_OPERATION("nop");

    private final String value;

    private EOperation(String value) {
        this.value = value;
    }
    
    public String getValue() {
        return value;
    }
    
    public static EOperation getOperation(String value) {
        for (EOperation operation : EOperation.values()) {
            if (operation.getValue().equals(value)) {
                return operation;
            }
        }
        throw new IllegalArgumentException("unknown value " + value);
    }
}