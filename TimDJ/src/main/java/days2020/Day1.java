package days2020;

import java.io.IOException;
import java.util.List;

import utils.FileReaderHelper;

/**
 * Day 1 of Advent of Code 2020
 *
 */
public final class Day1 {

	private void run() throws IOException {
		List<Integer> list = FileReaderHelper.readFileToInt("input2020/day1");
		int anwser = find2020(list);
		System.out.println(anwser);
		int anwser2 = find2020triple(list);
		System.out.println(anwser2);
	}

	private int find2020(List<Integer> list) {
		for (int int1 : list) {
			for (int int2 : list) {
				if (int1 + int2 == 2020) {
					return int1 * int2;
				}
			}
		}
		return 0;
	}

	private int find2020triple(List<Integer> list) {
		for (int int1 : list) {
			for (int int2 : list) {
				for (int int3 : list) {
					if (int1 + int2 + int3 == 2020) {
						return int1 * int2 * int3;
					}
				}
			}
		}
		return 0;
	}

	public static void main(String[] args) throws IOException {
		Day1 day = new Day1();
		day.run();
	}
}
