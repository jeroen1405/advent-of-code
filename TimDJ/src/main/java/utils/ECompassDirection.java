/*
 (C) COPYRIGHT 2020 TECHNOLUTION BV, GOUDA NL
| =======          I                   ==          I    =
|    I             I                    I          I
|    I   ===   === I ===  I ===   ===   I  I    I ====  I   ===  I ===
|    I  /   \ I    I/   I I/   I I   I  I  I    I  I    I  I   I I/   I
|    I  ===== I    I    I I    I I   I  I  I    I  I    I  I   I I    I
|    I  \     I    I    I I    I I   I  I  I   /I  \    I  I   I I    I
|    I   ===   === I    I I    I  ===  ===  === I   ==  I   ===  I    I
|                 +---------------------------------------------------+
+----+            |  +++++++++++++++++++++++++++++++++++++++++++++++++|
     |            |             ++++++++++++++++++++++++++++++++++++++|
     +------------+                          +++++++++++++++++++++++++|
                                                        ++++++++++++++|
                                                                 +++++|
 */
package utils;

/**
 * Enum for directions
 */
public enum ECompassDirection {
    NORTH(0),
    NORTH_EAST(45),
    EAST(90),
    SOUT_EAST(135),
    SOUTH(180),
    SOUTH_WEST(225),
    WEST(270),
    NORTH_WEST(315),
    UNSPECIFIED(-1);

    private final int degrees;

    private ECompassDirection(int degrees) {
        this.degrees = degrees;
    }

    public int getDegrees() {
        return degrees;
    }

    public static ECompassDirection getDirection(int degrees) {
        for (ECompassDirection direction : ECompassDirection.values()) {
            if (direction.getDegrees() == reduce(degrees)) {
                return direction;
            }
        }
        return UNSPECIFIED;
    }

    private static int reduce(int degrees) {
        if (degrees < 0) {
            return(degrees + 360);
        }
        
        if (degrees >= 360) {
            return(degrees - 360);
        }
        
        return degrees;
    }

    public static ECompassDirection getNewDirection(ECompassDirection currectDirection, ETurn turnDirection, int degrees) {
        int chaningDegrees = turnDirection == ETurn.LEFT ? -degrees : degrees;
        return getDirection(currectDirection.getDegrees() + chaningDegrees);
    }
}
