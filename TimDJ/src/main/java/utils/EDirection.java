package utils;

public enum EDirection {
	FORWARD ("forward"),
	DOWN ("down"),
	UP ("up");

	private final String directionName;
	
	private EDirection(String directionName) {
		this.directionName = directionName;
	}
	
	public String getDirectionName() {
		return directionName;
	}
	
	public static EDirection getDirection(String direct) {
		for (EDirection direction : EDirection.values()) {
			if (direction.getDirectionName().equals(direct)) {
				return direction;
			}
		}
		throw new IllegalArgumentException();
	}

}
