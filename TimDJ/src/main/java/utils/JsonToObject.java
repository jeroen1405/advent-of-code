package utils;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.google.common.base.Strings;

/**
 * Class to convert Api Responses to usable object
 */
public final class JsonToObject {

    private JsonToObject() {
        // Utility class
    }

    /**
     * Convert a JSON to a list of class objects
     * 
     * @param json the json that needs to be converted from te API
     * @param clazz The class type of the object to convert to
     * @return a list with the specified class objects or an empty list if there is no content available;
     * @throws IOException
     */
    public static <T> List<T> convert(String json, Class<T> clazz) throws IOException {
        if (Strings.isNullOrEmpty(json)) {
            return Collections.emptyList();
        }

        ObjectMapperProvider provider = new ObjectMapperProvider();
        ObjectMapper mapper = provider.getMapper();
        return parseList(json, mapper, clazz);
    }

    private static <T> List<T> parseList(String input, ObjectMapper mapper, Class<T> clazz) throws IOException {
        return mapper.readValue(input, listType(mapper, clazz));
    }

    private static <T> CollectionType listType(ObjectMapper mapper, Class<T> clazz) {
        return mapper.getTypeFactory().constructCollectionType(List.class, clazz);
    }
}
